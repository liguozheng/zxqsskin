/********************************************************************************
** Form generated from reading UI file 'zxqsskin.ui'
**
** Created by: Qt User Interface Compiler version 5.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ZXQSSKIN_H
#define UI_ZXQSSKIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_zxqsSkinClass
{
public:
    QWidget *centralWidget;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *zxqsSkinClass)
    {
        if (zxqsSkinClass->objectName().isEmpty())
            zxqsSkinClass->setObjectName(QStringLiteral("zxqsSkinClass"));
        zxqsSkinClass->resize(600, 400);
        centralWidget = new QWidget(zxqsSkinClass);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(60, 60, 75, 23));
        zxqsSkinClass->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(zxqsSkinClass);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 600, 23));
        zxqsSkinClass->setMenuBar(menuBar);
        mainToolBar = new QToolBar(zxqsSkinClass);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        zxqsSkinClass->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(zxqsSkinClass);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        zxqsSkinClass->setStatusBar(statusBar);

        retranslateUi(zxqsSkinClass);

        QMetaObject::connectSlotsByName(zxqsSkinClass);
    } // setupUi

    void retranslateUi(QMainWindow *zxqsSkinClass)
    {
        zxqsSkinClass->setWindowTitle(QApplication::translate("zxqsSkinClass", "zxqsSkin", 0));
        pushButton->setText(QApplication::translate("zxqsSkinClass", "PushButton", 0));
    } // retranslateUi

};

namespace Ui {
    class zxqsSkinClass: public Ui_zxqsSkinClass {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ZXQSSKIN_H
