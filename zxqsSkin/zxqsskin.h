#ifndef ZXQSSKIN_H
#define ZXQSSKIN_H

#include <QtWidgets/QMainWindow>
#include "ui_zxqsskin.h"
#include "zxqsSkinTheme.h"
class zxqsSkin : public QMainWindow
{
	Q_OBJECT

public:
	zxqsSkin(QWidget *parent = 0);
	~zxqsSkin();

private:
	Ui::zxqsSkinClass ui;
	zxqsSkinTheme* m_skin;
};

#endif // ZXQSSKIN_H
