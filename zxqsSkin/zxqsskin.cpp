#include "zxqsskin.h"

zxqsSkin::zxqsSkin(QWidget *parent)
	: QMainWindow(parent)
{
	ui.setupUi(this);
	m_skin = zxqsSkinThemeInstance();
	m_skin->load();
}

zxqsSkin::~zxqsSkin()
{

}
